# Intelligent Robotic Arm with Networking

Source for an autonomous networked robotic arm implemented on NetBurner's MOD5441x platform.
 
This could be useful if you are interested in controlling robotic servos, using the Pixy color recognition camera, a simple IR distance sensor, or set-up a access point from the Netburner platform. 

### Functionality

The robotic arm is designed to pick up objects. It can do this in an autonomous mode and a manual mode. In the autonomous mode, the robotic arm scans its surroundings looking for specific objects. Once it finds them, the arm will pick them up, place them to the side, and continue looking for other objects.

If a user connects to the Wifi access point and uses an Android phone app, they can override the autonomous mode and control the robotic arm manually. In this manual mode, the user can use the smartphone's gyroscope and the app's on-screen joystick to control the location of the arm.

### Useful Code

* Functions to communicate and control the Dynamixel AX-12 Robotic Servos. (`DynaServo.cpp`)
* Functions to control all five of the servos that make up the robotic arm to get the arm to move itself to specific coordinates and to retrieve objects. (`ArmController.cpp`)
* Functions to communicate with the Pixy color recognition camera and detect objects of specific colors (`pixy.cpp`)
* Functions that use the Analog-To-Digital Converters on the Netburner microprocessor(`SimpleAD.cpp`)
* Functions that create the TCP server and handles the data packets sent from the client (phone app) ('TCPServer')
* Functions that handle the higher-level behavior of the robotic arm incorporating all of its sensors and servos(`behaviorController.cpp`)


### Hardware

* [Pixy (CMUcam5) Smart Vision Sensor](http://charmedlabs.com/default/pixy-cmucam5/)
* [Sharp GP2Y0A41SK0F Analog Distance Sensor](https://www.pololu.com/product/2464/blog)
* [PhantomX Pincher Robot Arm Kit V2](http://www.trossenrobotics.com/p/PhantomX-Pincher-Robot-Arm.aspx)
* [802.11b/g/n Wireless Development Kit (External Antenna)](http://www.netburnerstore.com/product_p/nndk-nbwifiin-ufl-kit.htm)
* [NetBurner MOD5441X development kit](http://www.netburner.com/products/core-modules/mod5441x#kit)

### Notes

robotArmControllerWithWifi (https://bitbucket.org/netburnerinterns/robotArmControllerWithWifi)
Copyright (c) 2015 by

Benjamin Teng @ NetBurner, Inc. (http://www.netburner.com/)

Distributed under the BSD 3-Clause License
See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
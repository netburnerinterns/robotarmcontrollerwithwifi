/*
 * robotArmControllerWithWifi (https://bitbucket.org/netburnerinterns/robotArmControllerWithWifi)
 * Copyright (c) 2015 by
 *
 * Benjamin Teng @ NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#include <basictypes.h>
#include <math.h>

#include "ArmController.h"
#include "DynaServo.h"

//============ Constructor ====================
ArmController::ArmController(int fd_Serial1)
	: fd_Serial(fd_Serial1){

	uint8_t i = 0;
	for(i = 0; i < NUM_SERVOS; i++){
		servos[i].init(i, fd_Serial);
		positions[i] = CENTER_POS;
		speeds[i] = DEFAULT_SPEED;
	}

	servos[i].init((uint8_t)ALL_SERVO_ID, fd_Serial);

	lengths[0] = ARM_LEN_1;
	lengths[1] = ARM_LEN_2;
	lengths[2] = ARM_LEN_3;
}

// ===========Helper Functions ===================

// Sends all the desired positions and speeds to the transmit-all DynaServo
void ArmController::run(){
	servos[NUM_SERVOS].moveAllServos(positions,speeds);
}

// Moves the speed and position to default
void ArmController::returnHome(){
	for(int i = 0; i < NUM_SERVOS - 1; i++){
		positions[i] = CENTER_POS;
		speeds[i] = DEFAULT_SPEED;
	}
	run();
}

// Moves the servos to a position slightly away from the home position
void ArmController::position1(){
	positions[0] = CENTER_POS;
	for(int i = 1; i < NUM_SERVOS; i++){
		positions[i] = CENTER_POS + 50;
		speeds[i] = DEFAULT_SPEED;
	}
	run();
}

// Closes the Gripper end of robotic arm
void ArmController::grab(){
	positions[GRIP_SERVO] = 300;
	speeds[GRIP_SERVO] = 400;
	run();
}

// Opens the Gripper end of robotic arm
void ArmController::release(){
	positions[GRIP_SERVO] = CENTER_POS;
	speeds[GRIP_SERVO] = 900;
	run();
}

// Sets the speed of an individual servo
void ArmController::setSpeed(int whichServo, float speed){
	speeds[whichServo] = speed;
}


// Sets all the servos except for the gripper arm to the same speed
void ArmController::setAllSpeed(float speed){
	setSpeed(0,speed);
	setSpeed(1,speed);
	setSpeed(2,speed);
	setSpeed(3,speed);
}

//Converts the integer angle positions to radians that are centered correctly
float ArmController::convertToRads(uint16_t position){
	return (((float) position) * POS_TO_RADS) - ANGLE_OFFSET;
}

//Converts radian angle positions back into integer angles
uint16_t ArmController::convertToInts(float position){
	return (uint16_t)((position + ANGLE_OFFSET) / POS_TO_RADS);
}

// if isControlled is true, the tilt of the end of the arm is controlled and you can set its angle
// this will maintain the end of the arm at the target Coordinate
// If smoothing is set to true, then the end of the arm is defined at the joint rather than the end
// This allows a smoother movement when tilting because only the last servo is affected.
void ArmController::moveTo(Coordinate targetCoord, float targetRot, bool isControlled, float approachAngle, bool smoothing){

	// Convert the current servo positions to angles in radians centered properly
	float angles[3];
	angles[0] = convertToRads(positions[1]);
	angles[1] = convertToRads(positions[2]);
	angles[2] = convertToRads(positions[3]);
	float newAngles[3] = {angles[0], angles[1], angles[2]};


	float totReach = TOT_ARM_LEN;
	float numServosControlled = NUM_ARM_XY;

	// If the end servo is independently controlled, change the
	// max range and make sure that it isn't altered in code
	if(isControlled){

		// Could do trig to make it more intelligent
		totReach -= ARM_LEN_3;

		// To exclude control of the end servo
		numServosControlled = NUM_ARM_XY - 1;

		newAngles[2] = approachAngle;
	}

	Coordinate origin;
	origin.x = 0;
	origin.y = 0;
	float dist = distance(targetCoord,origin);

	// Checks to see if the robotic arm is trying to go to a coordinate that is out of reach
	if(dist >= totReach){
		targetCoord.x = totReach / dist * targetCoord.x;
		targetCoord.y = totReach / dist * targetCoord.y;
		printf("\r\nTarget Location exceeds maximum length reachable.\r\nNew Target is at x=%f y=%f\r\n", targetCoord.x,targetCoord.y);
	}

	// Prevents the arm from going below the surface it is sitting on
	if(targetCoord.y < -1 * OFFSET_HEIGHT + GRIPPER_LEN){
		targetCoord.y = -1 * OFFSET_HEIGHT + GRIPPER_LEN + 20;
		printf("\r\nTarget Location is attempting to go below the surface it is sitting on.\r\nNew Target is at x=%f y=%f\r\n", targetCoord.x,targetCoord.y);
	}

	dist = distance(get_xy(newAngles, smoothing),targetCoord);
	// Calculates iteratively the servo angles required to get the end of the arm to
	// the target coordinates
	while(fabs(dist) > THRESHOLD_FOR_MOVE_ALL){


		// Proportional Control: The error is the distance from the current location to the
		// target scaled by a gain value
		float scaleDist = dist * SCALE_DIFF;

		// Loop through three of the servos and move each of the servos such that the end of the arm
		// is closer to the target location
		for(int i = 0; i < numServosControlled; i++){
			float angTestPos[3] = {newAngles[0], newAngles[1], newAngles[2]};
			float angTestNeg[3] = {newAngles[0], newAngles[1], newAngles[2]};

			angTestPos[i] += STEP_SIZE;
			angTestNeg[i] -= STEP_SIZE;

			// Checks to see which direction would cause lower error between the target and the
			// current location
			float check = distance(targetCoord, get_xy(angTestPos,smoothing))
							- distance(targetCoord, get_xy(angTestNeg,smoothing));
			newAngles[i] = newAngles[i] - scaleDist * check;
		}
		dist = distance(get_xy(newAngles,smoothing),targetCoord);
	}

	// Converts the angles from radians back into discrete values
	positions[1] = convertToInts(newAngles[0]);
	positions[2] = convertToInts(newAngles[1]);
	positions[3] = convertToInts(newAngles[2]);

	positions[0] = convertToInts(targetRot);

	run();
	return;
}


void ArmController::rotTiltMove(float targetRot, float tiltAngle){
	positions[0] = convertToInts(targetRot);
	positions[3] = convertToInts(tiltAngle);

	run();
	return;
}

float ArmController::distance(Coordinate curr, Coordinate target){
	return sqrt(pow(target.x-curr.x, 2) + pow(target.y-curr.y, 2));
}

ArmController::Coordinate ArmController::get_xy(float angle[3], bool smoothing){

	Coordinate coord;

	coord.y = lengths[0] * cos(angle[0])
			+ lengths[1] * cos(angle[0] + angle[1]);

	coord.x = lengths[0] * sin(angle[0])
			+ lengths[1] * sin(angle[0] + angle[1]);

	if(!smoothing){
		coord.y += lengths[2] * cos(angle[0] + angle[1] + angle[2]);
		coord.x += lengths[2] * sin(angle[0] + angle[1] + angle[2]);
	}
	return coord;
}


void ArmController::changeLED(uint8_t id, uint8_t isOn){
	servos[id].changeLED(isOn);
}

float ArmController::getRotationPos(){
	return convertToRads(positions[0]);
}

float ArmController::getTiltAngle(){
	return convertToRads(positions[3]);
}

float ArmController::getPositions(uint8_t servoNum){
	return convertToRads(positions[servoNum]);
}

//==================== Various Print statements for debugging ============

void ArmController::print3s(float angles[3]){
	for(int i = 0; i < 3; i++){
		printf("Object %i: %f\r\n", i, angles[i]);
	}
}

void ArmController::printStatus(){
	iprintf("=======Printing the status of the ArmController=======\r\n");
	for(int i = 0; i < NUM_SERVOS; i++){
		printf("Target Position of Servo %i: %i", i, positions[i]);
		printf("Speed of Servo %i: %i\r\n\r\n", i, speeds[i]);
	}
}

void ArmController::Coordinate::printCoord(){
	printf("\r\nCoordinate X: %f, Coordinate Y: %f\r\n", x, y);
}



void ArmController::printServoStatus(){
	for(int i = 0; i < NUM_SERVOS; i++){
		printf("For Servo %i\r\n", i);
		servos[i].printServoStatus();
	}
}




















/*
 * robotArmControllerWithWifi (https://bitbucket.org/netburnerinterns/robotArmControllerWithWifi)
 * Copyright (c) 2015 by
 *
 * Benjamin Teng @ NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

//	This is the driver for the Dynamixel AX-12 Servos purchased from Trossen Robotics
//	@ http://www.trossenrobotics.com/dynamixel-ax-12-robot-actuator.aspx
//	This file sends the instructions required to control the servos




#ifndef __DYNA_SERVO_H
#define __DYNA_SERVO_H
#include <stdio.h>


//======Defined by Trossen Robotics===============
// These define statements came from the
// ax12.h files. They can be downloaded
// from the link below and are found in
// the arbotiX hardware files
//http://learn.trossenrobotics.com/arbotix/7-arbotix-quick-start-guide

#define AX12_MAX_SERVOS             30
#define AX12_BUFFER_SIZE            32

/** EEPROM AREA **/
#define AX_MODEL_NUMBER_L           0
#define AX_MODEL_NUMBER_H           1
#define AX_VERSION                  2
#define AX_ID                       3
#define AX_BAUD_RATE                4
#define AX_RETURN_DELAY_TIME        5
#define AX_CW_ANGLE_LIMIT_L         6
#define AX_CW_ANGLE_LIMIT_H         7
#define AX_CCW_ANGLE_LIMIT_L        8
#define AX_CCW_ANGLE_LIMIT_H        9
#define AX_SYSTEM_DATA2             10
#define AX_LIMIT_TEMPERATURE        11
#define AX_DOWN_LIMIT_VOLTAGE       12
#define AX_UP_LIMIT_VOLTAGE         13
#define AX_MAX_TORQUE_L             14
#define AX_MAX_TORQUE_H             15
#define AX_RETURN_LEVEL             16
#define AX_ALARM_LED                17
#define AX_ALARM_SHUTDOWN           18
#define AX_OPERATING_MODE           19
#define AX_DOWN_CALIBRATION_L       20
#define AX_DOWN_CALIBRATION_H       21
#define AX_UP_CALIBRATION_L         22
#define AX_UP_CALIBRATION_H         23
/** RAM AREA **/
#define AX_TORQUE_ENABLE            24
#define AX_LED                      25
#define AX_CW_COMPLIANCE_MARGIN     26
#define AX_CCW_COMPLIANCE_MARGIN    27
#define AX_CW_COMPLIANCE_SLOPE      28
#define AX_CCW_COMPLIANCE_SLOPE     29
#define AX_GOAL_POSITION_L          30
#define AX_GOAL_POSITION_H          31
#define AX_GOAL_SPEED_L             32
#define AX_GOAL_SPEED_H             33
#define AX_TORQUE_LIMIT_L           34
#define AX_TORQUE_LIMIT_H           35
#define AX_PRESENT_POSITION_L       36
#define AX_PRESENT_POSITION_H       37
#define AX_PRESENT_SPEED_L          38
#define AX_PRESENT_SPEED_H          39
#define AX_PRESENT_LOAD_L           40
#define AX_PRESENT_LOAD_H           41
#define AX_PRESENT_VOLTAGE          42
#define AX_PRESENT_TEMPERATURE      43
#define AX_REGISTERED_INSTRUCTION   44
#define AX_PAUSE_TIME               45
#define AX_MOVING                   46
#define AX_LOCK                     47
#define AX_PUNCH_L                  48
#define AX_PUNCH_H                  49

/** Status Return Levels **/
#define AX_RETURN_NONE              0
#define AX_RETURN_READ              1
#define AX_RETURN_ALL               2

/** Instruction Set **/
#define AX_PING                     1
#define AX_READ_DATA                2
#define AX_WRITE_DATA               3
#define AX_REG_WRITE                4
#define AX_ACTION                   5
#define AX_RESET                    6
#define AX_SYNC_WRITE               131

/** Error Levels **/
#define ERR_NONE                    0
#define ERR_VOLTAGE                 1
#define ERR_ANGLE_LIMIT             2
#define ERR_OVERHEATING             4
#define ERR_RANGE                   8
#define ERR_CHECKSUM                16
#define ERR_OVERLOAD                32
#define ERR_INSTRUCTION             64

/** AX-S1 **/
#define AX_LEFT_IR_DATA             26
#define AX_CENTER_IR_DATA           27
#define AX_RIGHT_IR_DATA            28
#define AX_LEFT_LUMINOSITY          29
#define AX_CENTER_LUMINOSITY        30
#define AX_RIGHT_LUMINOSITY         31
#define AX_OBSTACLE_DETECTION       32
#define AX_BUZZER_INDEX             40





//=======Written by Netburner=====================

#define START_BIT					0xff

#define LED_MSG_LEN					4
#define MOVE_SERVO_MSG_LEN			7

#define READ_DATA_MSG_LEN			4

#define NUM_SERVOS					5

// From AX-12 Documentation:
// Length = (L+1) * N + 4
// L = Data Length
// N = Number of Servos
// Designed for N = 5
// and L = 4
// This represents L
#define MOVE_ALL_DATA_LEN			4
#define MOVE_ALL_SERVO_MSG_LEN		(MOVE_ALL_DATA_LEN + 1) * NUM_SERVOS + MSG_OVERHEAD_LEN

#define MSG_OVERHEAD_LEN			4

// Set by the AX-12 Documentation
#define MAX_ANGLE					0x03ff
#define MAX_SPEED					0x03ff

// When this is sent over SPI, all the servos will move
#define ALL_SERVO_ID				0xFE

#define MIN_SERVO3_ANGLE	50
#define MAX_SERVO3_ANGLE	890



class DynaServo {

	// The Instruction Packet sent from the controller to the servo
	struct TXMessage {
		uint16_t startchunk;
		uint8_t id;
		uint8_t len;
		uint8_t inst;
		uint8_t param[];

		void setCheckSum();
		void printTX();
	};

	// The Status Packet sent from the Servo to the controller
	struct RXMessage {
		uint16_t startchunk;
		uint8_t id;
		uint8_t len;
		uint8_t error;
		uint8_t param[];

		void printRX();
		uint8_t calcCheckSum();
	};


	uint8_t id;

	// Required File Descriptor
	int fd_Serial;



	// Member Functions
	void transmit(TXMessage *msg);
	RXMessage receive();

	public:
		struct ServoStatus {
			uint16_t position;
			uint16_t speed;
			uint8_t loadDir;
			uint16_t loadVal;

		};
		void init(uint8_t id, int fd_Serial);
		RXMessage readData(uint8_t startAddr, uint8_t numBytesToRead);

		BOOL checkLEDStatus();
		void printServoStatus();

		void servoTest();

		uint16_t checkServoPos(uint8_t idServo,uint16_t position);

		void changeLED(uint8_t isOn);
		void moveServo(uint16_t position, uint16_t speed);

		ServoStatus getServoStatus();

		BOOL moveAllServos(uint16_t* positions, uint16_t* speeds);

		// Constructor
		DynaServo();
		DynaServo(uint8_t id, int fd_Serial);

};

#endif


























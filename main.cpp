/*
 * robotArmControllerWithWifi (https://bitbucket.org/netburnerinterns/robotArmControllerWithWifi)
 * Copyright (c) 2015 by
 *
 * Benjamin Teng @ NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#include "predef.h"
#include <stdio.h>
#include <ctype.h>
#include <startnet.h>
#include <autoupdate.h>
#include <dhcpclient.h>
#include <smarttrap.h>
#include <taskmon.h>
#include <NetworkDebug.h>
#include <serial.h>
#include <math.h>
#include <init.h>
#include <serial.h>
#include <pins.h>
#include <dspi.h>

#include <nbwifi/nbWifi.h>
#include <dhcpd.h>
#include "TCPServer.h"


#include "SimpleAD.h"
#include "behaviorController.h"
#include "DynaServo.h"
#include "ArmController.h"
#include "pixy.h"

extern "C" {
void UserMain(void * pd);
}

const char * AppName="robotArmController";


// The half-duplex UART for the servo
#define UART_NUM 0

// Opens the correct serial port
#define SERIALPORT_TO_USE		2
#define BAUDRATE_TO_USE			115200
#define	STOP_BITS				1
#define DATA_BITS				8


void UserMain(void * pd) {

	iprintf("Enter UserMain\r\n");
    init();

    iprintf( "%s application started\r\n", AppName );

    InitSingleEndAD();

	#ifdef _DEBUG
    InitializeNetworkGDB();
	#endif

    // Initialize pins needed for DSPI
#ifdef NANO54415
    Pins[31].function(PIN_31_DSPI1_SCK);
    Pins[33].function(PIN_33_DSPI1_SIN);
    Pins[35].function(PIN_35_DSPI1_SOUT);
    Pins[37].function(PIN_37_GPIO);
    Pins[37] = 0;		//Puts SPI1 CS to low allowing RX and TX
    // Create and initialize semaphore for DSPI (optional)
	Pins[10].function(PIN_10_GPIO);
	Pins[10] = 1;

#elif defined MOD5441X
    J2[21].function(PINJ2_21_DSPI3_SIN);
    J2[22].function(PINJ2_22_DSPI3_SOUT);
    J2[23].function(PINJ2_23_DSPI3_PCS0);
    J2[24].function(PINJ2_24_DSPI3_SCK);
#endif

    InitSingleEndAD();

    //Close the serial port in case it is already open.
    SerialClose( SERIALPORT_TO_USE );

    //Open the serial comm port to the computer
    int fdserial = OpenSerial( SERIALPORT_TO_USE,
                               BAUDRATE_TO_USE,
                               STOP_BITS,
                               DATA_BITS,
                               eParityNone );
    ReplaceStdio( 0, fdserial );
    ReplaceStdio( 1, fdserial );
    ReplaceStdio( 2, fdserial );

    // Sets up the semaphore required for SPI communication
    // to the PixyCam
    InitPixy();


    iprintf("Application started\n");

    // Sets up the Wifi Module
    InitAP_SPI();
    iprintf("Starting Access Point DHCP server... ");

    if (AddStandardDHCPServer(theWifiIntf->GetSystemInterfaceNumber())) {
        iprintf("Success\n\n");
    } else {
        iprintf("Error: another server exists\n\n");
    }
    StartTCPServerTask();

    // Sets up the UART communication to control the Dynamixel digital servos
    int fdSerial2 = OpenSerial(UART_NUM,BAUDRATE_TO_USE,STOP_BITS,DATA_BITS,eParityNone);
	fd_set read_fds;
	FD_ZERO( &read_fds );
	FD_SET( fdSerial2, &read_fds );
	Serial485HalfDupMode(UART_NUM, 1);



	ArmController controller(fdSerial2);

	// Nod to show the arm is working
	iprintf("Returning Home \r\n==================\r\n");
	iprintf("Scanning1\r\n");
	controller.position1();
	OSTimeDly(5);
	controller.returnHome();
	OSTimeDly(20);

	// The robot will be scan for familiar objects and try to pick them up
	// If a phone is connected to the Wifi Access Point called RobotServer
	// and the app is used, the robot will be controlled by the phone
	scanTrackRoutine(&controller);

	while(1){
		iprintf("Looping\r\n");
		OSTimeDly(1);
	}

}

































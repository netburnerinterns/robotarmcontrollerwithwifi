/*
 * robotArmControllerWithWifi (https://bitbucket.org/netburnerinterns/robotArmControllerWithWifi)
 * Copyright (c) 2015 by
 *
 * Benjamin Teng @ NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#ifndef _ARM_CONTROLLER_H
#define _ARM_CONTROLLER_H

#include "DynaServo.h"

#define CENTER_POS					511
#define DEFAULT_SPEED				250

#define GRIP_SERVO					4

// Number of brackets that hold each of the joints together
#define NUM_ARM_LEN					3

// Number of arms that rotate in the XY plane
#define NUM_ARM_XY					3

// Length of each of the brackets in mm
#define ARM_LEN_1					104
#define ARM_LEN_2					104
#define ARM_LEN_3					77
#define TOT_ARM_LEN					285

#define ANGLE_OFFSET				2.617993878

#define OFFSET_HEIGHT				137
#define GRIPPER_LEN					30

#define POS_TO_RADS					0.0051132693

#define STEP_SIZE					0.001
#define SCALE_DIFF					0.006

#define THRESHOLD_FOR_MOVE_ALL		2


class ArmController {
	// Required File Descriptors
	int fd_Serial;

	// NUM_SERVOS is the number of servos connected
	// and the additional DynaServo is the all
	// transmit servo
	// For the robotic arm:
	// ID: 0 is the base servo that controls the rotation of the robotic arm
	// ID: 1, 2, 3 are the servos that determine the height and horizontal distance of the
	// gripper end
	// ID: 4 controls the gripper.
	// ID: 254 is the transmit all servo and when it is written to, all the connected servos
	// will respond to it
	DynaServo servos[NUM_SERVOS + 1];

	uint16_t positions[NUM_SERVOS];
	uint16_t speeds[NUM_SERVOS];

	float lengths[NUM_ARM_LEN];

	void printStatus();
	void print3s(float angles[3]);

	public:
		struct Coordinate{
			float x;
			float y;
			void printCoord();

		};

		ArmController(int fd_Serial);

		void moveTo(Coordinate coord, float rot, bool isControlled = 0, float approach = 0, bool smoothing = 0);
		Coordinate get_xy(float angle[3], bool smoothing = 0);

		void returnHome();
		void position1();
		void grab();
		void release();

		void run();


		Coordinate get_xy2arm(float angle[3]);

		void setSpeed(int whichServo, float speed);
		void setAllSpeed(float speed);

		float distance(Coordinate curr, Coordinate target);

		void changeLED(uint8_t id, uint8_t isOn);

		void printServoStatus();
		float getRotationPos();
		float getTiltAngle();
		float getPositions(uint8_t servoNum);

		void rotTiltMove(float targetRot, float tiltAngle);
		float convertToRads(uint16_t position);
		uint16_t convertToInts(float position);

};

#endif



/*
 * RobotTCPServer (https://bitbucket.org/netburnerinterns/robottcpserver)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#include "predef.h"

#include <tcp.h>
#include <startnet.h>
#include <utils.h>

#include "TCPServer.h"

#define TCP_LISTEN_PORT 23
#define DATA_BYTES sizeof(DataPacket)

DWORD TcpServerTaskStack[USER_TASK_STK_SIZE];

typedef struct {
    float yaw;
    float pitch;
    float roll;
    char joy_x;
    char joy_y;
    BYTE grab;
    BYTE cntrl;
} DataPacket;

static DataPacket* data;
static bool connected = false;

bool ClientConnected() {
    return connected;
}

float GetYaw() {
    return data->yaw;
}

float GetPitch() {
    return data->pitch;
}

float GetRoll() {
    return data->roll;
}

int GetJoyX() {
    return (int) data->joy_x;
}

int GetJoyY() {
    return (int) data->joy_y;
}

bool GetGrab() {
    return (bool) data->grab;
}

bool GetControlScheme() {
    return (bool) data->cntrl;
}

void IPtoString(IPADDR ia, char *s) {
    PBYTE ipb = (PBYTE)&ia;
    siprintf(s, "%d.%d.%d.%d",
            (int)ipb[0], (int)ipb[1], (int)ipb[2], (int)ipb[3]);
}

/*
 * Reads and parses data from a TCP socket until the client disconnects
 */
void ParseData(int fdnet) {
    BYTE control_data[DATA_BYTES];
    bool disconnected = false;

    while(!disconnected) {
        int n = 0;
        int remaining_bytes = DATA_BYTES;

        while(remaining_bytes > 0) {
            // if the client closes the connection,
            // read() returns a negative value

            n = read(fdnet, (char*) (control_data + n), remaining_bytes - n);

            if (n < 0) {
                disconnected = true;
                break;
            }

            remaining_bytes -= n;
        }

        data = (DataPacket*) control_data;
        ShowData((PBYTE)data, DATA_BYTES);

        //printf("Yaw: %f\nPitch: %f\nRoll: %f\n\n", yaw, pitch, roll);
    }
}

/*
 * Runs a TCP server on the specified port
 * Adapted from NetBurner TCP server example
 */
void TcpServerTask(void* pd) {
    int ListenPort = (int) pd;

    // Set up the listening TCP socket
    int fdListen = listen(INADDR_ANY, ListenPort, 5);

    if (fdListen > 0) {
        IPADDR  client_addr;
        WORD    port;

        while(1) {
            // The accept() function will block until a TCP client requests
            // a connection. Once a client connection is accepted, the
            // file descriptor fdnet is used to read from it

            iprintf( "Waiting for connection on port %d...\n\n", ListenPort );
            int fdnet = accept(fdListen, &client_addr, &port, 0);

            iprintf("Connected to: ");
            ShowIP(client_addr);
            iprintf(":%d\n\n", port);

            connected = true;

            // while the connection is open
            while (fdnet > 0) {

                // block until the client disconnects
                ParseData(fdnet);

                connected = false;

                // close the connection

                iprintf("Closing client connection: ");
                ShowIP(client_addr);
                iprintf(":%d\n", port);

                close(fdnet);
                fdnet = 0;
            }
        }
    }
}

void StartTCPServerTask() {
    if (OSTaskCreate(TcpServerTask,
            (void*)TCP_LISTEN_PORT,
            &TcpServerTaskStack[USER_TASK_STK_SIZE] ,
            TcpServerTaskStack,
            MAIN_PRIO-1)!= OS_NO_ERR) {
        iprintf("Error starting TCP server task\n");
    }
}

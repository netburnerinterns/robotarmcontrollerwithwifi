/*
 * robotArmControllerWithWifi (https://bitbucket.org/netburnerinterns/robotArmControllerWithWifi)
 * Copyright (c) 2015 by
 *
 * Benjamin Teng @ NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#ifndef BEHCON_H_
#define BEHCON_H_

#include <serial.h>
#include <math.h>
#include <pins.h>

#include "DynaServo.h"
#include "ArmController.h"
#include <nbwifi/nbWifi.h>
#include "TCPServer.h"
#include <dhcpd.h>
#include "pixy.h"
#include <init.h>
#include <serial.h>
#include <pins.h>
#include <dspi.h>
#include "SimpleAD.h"

// The distance between the sensors and the center of the claw in millimeters
#define CLAW_TO_DIST_SENSOR		42
#define CLAW_TO_CAM				95

#define DEFAULT_OBJ_HEIGHT		-100

//The center of the PixyCam
#define CENTER_X  				160
#define CENTER_Y  				100
#define OFFSET_CENTER_Y  		150

//The SPI Module for the Pixy
#define SPI_MODULE				3
#define MAX_BLOCKS_PER_FRAME 	90

//Constants used for the IR distance sensor calculation
#define SCALAR_FIT				127.33528
#define POWER_FIT				-1.0849237314

// The offset to the target coordinate to ensure that the robotic arm
// has a good grip on the objects
#define GRIPPER_ARM_OFFSET		10
//The number of elements averaged together in the averaging filter
#define AVG_SIZE				20

// The threshold for how centered the object is
#define CAMERA_THRESH		5
#define MAX_TILT			2

#define SCAN_SPEED		8 * POS_TO_RADS
#define SCAN_RANGE		1.57

// Smallest size that counts as an object in pixels
#define MIN_OBJ_SIZE	300

// The states of the finite state machine for the
// scanTrackRoutine function
#define ST_SCAN			1
#define ST_TRACK		2
#define ST_OFFSETCAM	3
#define ST_GRAB			4
#define ST_PLACE		5
#define ST_OVERRIDE		6

// The amount that phone joystick values are scaled
#define SCALE_PHONE_IN	.6

#define LOWER_ARM_BOUND	-20

void InitPixy(void);
uint8_t getByte(void);
uint8_t getByte(uint8_t out);
int getFrame();
void printFrame(int numBlocks);
void defaultScanPose(ArmController* controller);
float readRangeSensor();
ArmController::Coordinate calcObjCoordPixy(ArmController* controller, float distance);
ArmController::Coordinate calcObjCoordDist(ArmController* controller, float distance);
BOOL followObj(ArmController* controller, int yCenter);
void scanning(ArmController* controller);
void phoneOverride(ArmController* controller);
void scanTrackRoutine(ArmController* controller);
void blinkLEDSeq(ArmController controller);

void controlSchemeZero(ArmController* controller);
void controlSchemeOne(ArmController* controller);


#endif /* BEHCON_H_ */

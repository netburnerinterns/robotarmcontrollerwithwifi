/*
 * RobotTCPServer (https://bitbucket.org/netburnerinterns/robottcpserver)
 * Copyright (c) 2015 by
 *
 * Sam Posner (http://arcadeoftheabsurd.com/)
 * and NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#ifndef TCPSERVER_H_
#define TCPSERVER_H_

float GetYaw();
float GetPitch();
float GetRoll();

int GetJoyX();
int GetJoyY();

bool GetGrab();
bool GetControlScheme();

bool ClientConnected();
void StartTCPServerTask();

#endif /* TCPSERVER_H_ */

/*
 * robotArmControllerWithWifi (https://bitbucket.org/netburnerinterns/robotArmControllerWithWifi)
 * Copyright (c) 2015 by
 *
 * Benjamin Teng @ NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */


#include "behaviorController.h"

float scanDirection = 1;


//============Helper Functions for the PixyCam======================

// Stores all the blocks last captured by the PixyCam
Block* allBlocks[MAX_BLOCKS_PER_FRAME];
static BYTE RXBuffer[1], TXBuffer[1];

//// Create and initialize semaphore for DSPI
OS_SEM DSPI_SEM;


//Initializes the semaphore and the SPI module required for the Pixy
void InitPixy(){
	OSSemInit(& DSPI_SEM, 0);
	DSPIInit(SPI_MODULE);
	Pinit();
}

// Receives a single byte from the PixyCam
uint8_t getByte(void)
{
	DSPIStart(SPI_MODULE, NULL,
			RXBuffer, 1, &DSPI_SEM);              // Send data via DSPI
	OSSemPend( &DSPI_SEM, 0 );                      // Wait for DSPI to complete

	return RXBuffer[0];
}

// Receives and Transmits a single byte to the PixyCam
uint8_t getByte(uint8_t out)
{
	TXBuffer[0] = out;

	DSPIStart(SPI_MODULE, TXBuffer,
			RXBuffer, 1, &DSPI_SEM);              // Send data via DSPI
	OSSemPend( &DSPI_SEM, 0 );                      // Wait for DSPI to complete

	return RXBuffer[0];
}

// Finds all the blocks in the frame and stores them in the global variable allBlocks
int getFrame()
{
	int numBlocks = getBlocks(MAX_BLOCKS_PER_FRAME);
	if(numBlocks)
	{
		// Populates allBlocks with the blocks of the frame
		for(int i = 0; i < numBlocks; i++)
		{
			allBlocks[i] = getBlock(i);
		}

//		printFrame();
	}
	return numBlocks;
}

// Prints the data within each of the blocks captured by the Pixy
void printFrame(int numBlocks){
	for(int i = 0; i < numBlocks; i++)
	{
		iprintf("Block number: %d,Location: x,y=%d,%d, Total Size: %d, Signature: %d\r\n",
				i,allBlocks[i]->x,allBlocks[i]->y,
				allBlocks[i]->width * allBlocks[i]->height, allBlocks[i]->signature);
	}
}


//======Functions to control the robotic arm===================================

// The default robot pose used for scanning for objects
void defaultScanPose(ArmController* controller){
	ArmController::Coordinate coord1;
	coord1.x = 50;
	coord1.y = 50;
	float ang1 = 0;
	float tilt1 = 1.7;
	controller->moveTo(coord1,ang1,1,tilt1);
}

// Reads the value of Infrared Distance Sensor
float readRangeSensor(){
	StartAD();
	while (!ADDone())
		asm("nop");
	// Converts the value given by the ADC to the voltage value at the pin
	float rangeVal = ((float) GetADResult(0)) * 3.3 / (32768.0);
	return rangeVal;
}

// The values were gained from the Datasheet for the IR range sensor
// for the SHARP GP2Y0A41SK0F Distance measuring sensor unit. A curve was
// described in the datasheet and a power function best fit it.
// This function will return the number of mm the sensor is from the object
// This function converts the voltage reading from the sensor and returns
// the distance in mm.
float readDistance(){
	return SCALAR_FIT * pow(readRangeSensor(), POWER_FIT);
}

// Uses the Distance Sensor to approximate the coordinates of the target object
ArmController::Coordinate calcObjCoordDist(ArmController* controller, float distance){
	ArmController::Coordinate objLocation;
	objLocation.x = 0;
	objLocation.y = 0;

//	Approximates the Coordinates of the object AVG_SIZE number of times and averages it
	for(int i = 0; i < AVG_SIZE; i++){

		// Gets the servo positions to calculate the location
		//and tilt angle of the distance sensor
		// This excludes the servo that controls the gripper and
		// servo that rotates the arm
		float servoPos[NUM_SERVOS - 2];
		for(int i = 0; i < NUM_SERVOS - 2; i++){
			servoPos[i] = controller->getPositions(i+1);
		}

		// The angle of the end of the arm
		float totAng = servoPos[0] + servoPos[1] + servoPos[2];

		float sensorAngle = totAng - M_PI/2;

		float xDist = CLAW_TO_DIST_SENSOR * sin(sensorAngle);
		float yDist = CLAW_TO_DIST_SENSOR * cos(sensorAngle);
		ArmController::Coordinate gripEnd = controller->get_xy(servoPos);

		// This sets xDist and yDist to the location of the distance sensor from the origin
		xDist += gripEnd.x;
		yDist += gripEnd.y;

		// The angle that the distance sensor is pointed where 0 is pointed directly downwards
		float camAngle = M_PI - totAng;

		// Location of the object
		float xObj = distance * sin(camAngle);
		float yObj = distance * cos(camAngle);

		float xObj2 = xObj + xDist;
		float yObj2 = yDist - yObj;


		objLocation.x += xObj2;
		objLocation.y += yObj2;
	}

	// Averages the Coordinates and adds a small offset to get the arm in the correct location
	objLocation.x = objLocation.x / AVG_SIZE;
	objLocation.y /= AVG_SIZE;

	return objLocation;
}



// Moves the robotic arm to to make the object more centered in the PixyCam's view
BOOL followObj(ArmController* controller, int yCenter){
	// The blocks in the array are ordered by size
	// This stores the largest block in the array as "block"
	Block* block = allBlocks[0];

	// Calculates how far away the object is from centered
	float YDiff = ((float) block->y) - yCenter;
	float XDiff = ((float) block->x) - CENTER_X;

	// Sets the new angle of the servos to be slightly closer to centering the object
	float rotAng = controller->getRotationPos()- XDiff/1500;
	float tiltAng = controller->getTiltAngle() + YDiff/1500;

	// Ensures that the servo positions are bounded
	if(tiltAng > MAX_TILT){
		tiltAng = MAX_TILT;
	}

	controller->rotTiltMove(rotAng,tiltAng);

	// Returns true if the object is within a threshold of centered
	if(fabs(XDiff) < CAMERA_THRESH & fabs(YDiff) < CAMERA_THRESH){
		return TRUE;
	}
	return FALSE;
}


// Moves the robotic arm to scan back and forth
void scanning(ArmController* controller){
	float rotAng = controller->getRotationPos();

	if(rotAng > SCAN_RANGE){
		scanDirection = -1;
	}
	else if(rotAng < -1 * SCAN_RANGE){
		scanDirection = 1;
	}
	controller->rotTiltMove(rotAng + SCAN_SPEED * scanDirection, controller->getTiltAngle());
}



// Scans for objects and then will focus the camera on it
void scanAndTrackObj(ArmController* controller, ArmController::Coordinate coord1){
	// If there is an object found by the Pixy, follow it. Otherwise keep scanning
	if(getFrame()){
		Block* block = allBlocks[0];
		if(block->width * block->height > MIN_OBJ_SIZE){
			followObj(controller, CENTER_Y);
		}
	}
	else{
		scanning(controller);
	}
}

// Controls the robotic arm based on the information sent by the phone app
// There are two different control schemes that can be selected by the app.
void phoneOverride(ArmController* controller){


	//Lowers the speed of the servos to make the robot movements more smooth.
	float newSpeed = 100;
	controller->setAllSpeed(newSpeed);


	iprintf("A Phone has taken control\r\n");


	if(GetControlScheme()){
		// Control Scheme 1
		controlSchemeOne(controller);

	}
	// Control Scheme 0
	else{
		controlSchemeZero(controller);
	}
	// Sets the speeds back to default
	controller->setAllSpeed(DEFAULT_SPEED);
	defaultScanPose(controller);
	controller->release();
}


// Controls the robot like an arcade grabber.
// Its arm will always be facing downwards.
// Pitch: Changes the y coordinate(height) of the gripper
// JoystickX: Changes the rotation/ the direction that the arm is facing
// JoystickY: Changes the x coordinate(horizontal location) of the gripper
void controlSchemeZero(ArmController* controller){
	// This is the starting location of the end of the arm
	ArmController::Coordinate targetCoord;
	targetCoord.x = 104;
	targetCoord.y = 104;

	float rotAngle = 0;
	float tiltAngle = 0;
	float joyX = 0;
	float joyY = 100;
	while(ClientConnected()){

		OSTimeDly(TICKS_PER_SECOND/20);

		joyX += GetJoyX();
		joyY += GetJoyY();

		rotAngle += (-1 * 0.004 * joyX - rotAngle)/5;
		targetCoord.x = -1 * SCALE_PHONE_IN * joyY + 200;
		tiltAngle = 5 * M_PI / 6 - (controller->getPositions(1) + controller->getPositions(2));
		targetCoord.y = GetPitch() * -300;

		if(targetCoord.y < LOWER_ARM_BOUND){
			targetCoord.y = LOWER_ARM_BOUND;
		}

		controller->moveTo(targetCoord, rotAngle, 1, tiltAngle,1);

		if(GetGrab()){
			controller->grab();
		}
		else{
			controller->release();
		}
	}
}

// Controls the robot with:
// Pitch: Changes the angle of the gripper end
// Yaw: Changes the direction the robot is facing
// JoystickX: Changes the y coordinate(height) of the gripper
// JoystickY: Changes the x coordinate(horizontal location) of the gripper
// These coordinates are purposefully flipped due to the way the Coordinate system
// has been defined
void controlSchemeOne(ArmController* controller){
	// This is the starting location of the end of the arm
	ArmController::Coordinate targetCoord;
	targetCoord.x = 104;
	targetCoord.y = 104;

	float rotAngle = 0;
	float tiltAngle = 0;
	while(ClientConnected()){
		OSTimeDly(TICKS_PER_SECOND/20);

		targetCoord.x += SCALE_PHONE_IN * GetJoyY();
		targetCoord.y += SCALE_PHONE_IN * GetJoyX();

		rotAngle += (-1 * GetYaw() - rotAngle)/5;
		tiltAngle += (GetPitch() + M_PI/4 - tiltAngle)/5;

		controller->moveTo(targetCoord, rotAngle, 1, tiltAngle,1);

		if(GetGrab()){
			controller->grab();
		}
		else{
			controller->release();
		}
	}
}

// Scans for specific objects and picks them up when found.
// A phone can override the autonomous routine and control it from an app.
void scanTrackRoutine(ArmController* controller){

	int currState = ST_SCAN;
	ArmController::Coordinate objCoord;
	objCoord.x = 100;
	objCoord.y = 100;
	BOOL foundObj = 0;

	defaultScanPose(controller);

	while(1)
	{
		OSTimeDly(TICKS_PER_SECOND/20);

		switch(currState){
			case ST_SCAN:
				if(ClientConnected()){
					currState = ST_OVERRIDE;
				}
				scanning(controller);
				if(getFrame()){
					currState = ST_TRACK;
				}
				break;


			case ST_TRACK:
				if(getFrame()){
					Block* block = allBlocks[0];
					if(block->width * block->height > MIN_OBJ_SIZE){
						foundObj = followObj(controller,CENTER_Y);

						// Checks to see if the Coordinates returned were legitimate
						// If they are legitimate, it means the camera is centered on
						// the object
						if(foundObj){
							printf("Signature Found is %i\r\n", block->signature);
							if(block->signature == 1){
								currState = ST_OFFSETCAM;
							}
						}

					}
				}
				else
					currState = 1;
				break;

			case ST_OFFSETCAM:
				if(getFrame()){
					Block* block = allBlocks[0];
					if(block->width * block->height > MIN_OBJ_SIZE){
						foundObj = followObj(controller, OFFSET_CENTER_Y);

						// Checks to see if the Coordinates returned were legitimate
						// If they are legitimate, it means the camera is centered on
						// the object
						if(foundObj){
							printf("Signature Found is %i\r\n", block->signature);
							if(block->signature == 1){
								objCoord = calcObjCoordDist(controller, readDistance());
								currState = ST_GRAB;
							}
						}
					}
				}
				else
					currState = ST_SCAN;
				break;

			case ST_GRAB:
				controller->moveTo(objCoord,controller->getRotationPos());
				OSTimeDly(TICKS_PER_SECOND);
				controller->grab();
				currState = ST_PLACE;
				break;

			case ST_PLACE:
				defaultScanPose(controller);
				OSTimeDly(TICKS_PER_SECOND);
				controller->rotTiltMove(-1.7,1.7);
				OSTimeDly(TICKS_PER_SECOND);
				controller->release();
				OSTimeDly(TICKS_PER_SECOND);
				defaultScanPose(controller);
				OSTimeDly(TICKS_PER_SECOND);
				currState = ST_SCAN;
				break;

			case ST_OVERRIDE:
				phoneOverride(controller);
				currState = ST_SCAN;
				break;
			default:
				iprintf("\r\nCurrent state does not match possible states.\r\n");
				return;
		}
	}
}



// Blinks the LEDs on the each of the visible servos
void blinkLEDSeq(ArmController controller){
	// LED Sequence
	controller.changeLED(1,1);
	iprintf("Set On\n");
	OSTimeDly(TICKS_PER_SECOND/2);
	iprintf("Set Off\n");
	controller.changeLED(1,0);
	OSTimeDly(TICKS_PER_SECOND/2);

	controller.changeLED(2,1);
	iprintf("Set On\n");
	OSTimeDly(TICKS_PER_SECOND/2);
	iprintf("Set Off\n");
	controller.changeLED(2,0);
	OSTimeDly(TICKS_PER_SECOND/2);

	controller.changeLED(3,1);
	iprintf("Set On\n");
	OSTimeDly(TICKS_PER_SECOND/2);
	iprintf("Set Off\n");
	controller.changeLED(3,0);
	OSTimeDly(TICKS_PER_SECOND/2);

	controller.changeLED(4,1);
	iprintf("Set On\n");
	OSTimeDly(TICKS_PER_SECOND/2);
	iprintf("Set Off\n");
	controller.changeLED(4,0);
	OSTimeDly(TICKS_PER_SECOND/2);
}


/*
 * robotArmControllerWithWifi (https://bitbucket.org/netburnerinterns/robotArmControllerWithWifi)
 * Copyright (c) 2015 by
 *
 * Benjamin Teng @ NetBurner, Inc. (http://www.netburner.com/)
 *
 * Distributed under the BSD 3-Clause License
 * See LICENSE.txt or visit http://opensource.org/licenses/BSD-3-Clause
 */

#include <basictypes.h>
#include <startnet.h>
#include "DynaServo.h"

//===============Constructors=======================================
DynaServo::DynaServo()
	: id(0), fd_Serial(0){
	//Do nothing
}

DynaServo::DynaServo(uint8_t id, int fd_Serial)
	: id(id), fd_Serial(fd_Serial){
	// Do nothing
}

// Transmits the message that is passed
void DynaServo::transmit(TXMessage *msg){

		uint8_t bufferSize = msg->len + MSG_OVERHEAD_LEN;
		// Adds the checksum to the message
		msg->setCheckSum();
		// Transmits the whole message
		writeall(fd_Serial,(char *) msg, bufferSize);
}


//===============General Use Functions=======================================
void DynaServo::init(uint8_t idNum, int fd_SerialVal){
	id = idNum;
	fd_Serial = fd_SerialVal;
}


// Receives a Status Packet from the AX12 servo and returns it as a RXMessage
DynaServo::RXMessage DynaServo::receive(){

	char buffer[50];
	RXMessage *msg = (RXMessage *)buffer;

	fd_set read_fds;
	FD_ZERO( &read_fds );
	FD_SET( fd_Serial, &read_fds );

	/*
	  select() will block until any fd has data to be read, or
	  has an error. When select() returns, read_fds and/or
	  error_fds variables will have been modified to reflect
	  the events.
	 */
	if(select(FD_SETSIZE,&read_fds, (fd_set *) 0, (fd_set *) 0, TICKS_PER_SECOND * 5 ))
	{
		// If the listen fd has a connection request, accept it.
		if (FD_ISSET( fd_Serial, &read_fds ) )
		{
			int nread = 0;
			// Reads the start chunk, ID, and the length
			while(nread < MSG_OVERHEAD_LEN)
			{
				nread +=  read( fd_Serial, (char *) msg + nread, MSG_OVERHEAD_LEN - nread);
			}
			// Uses the length of the message to read the error message, all the parameters, and the checksum
			while(nread < msg->len + MSG_OVERHEAD_LEN){
				nread +=  read( fd_Serial, (char *) msg + nread, msg->len + MSG_OVERHEAD_LEN - nread);
			}
		}
	}
	return *msg;
}

// Reads the registers on the servo
DynaServo::RXMessage DynaServo::readData(uint8_t startAddr, uint8_t numBytesToRead){
	char buffer[READ_DATA_MSG_LEN + MSG_OVERHEAD_LEN];
	TXMessage *TXmsg = (TXMessage *)buffer;

	TXmsg->startchunk = 0xffff;
	TXmsg->id = id;
	TXmsg->len = READ_DATA_MSG_LEN;
	TXmsg->inst = AX_READ_DATA;
	TXmsg->param[0] = startAddr;
	TXmsg->param[1] = numBytesToRead;

	transmit(TXmsg);

	return receive();
}


// Calculates and sets the checksum on an TXmessage
void DynaServo::TXMessage::setCheckSum(){
	uint8_t paramTot = 0;
	uint8_t i = 0;
	uint8_t numParams = len - 2;

	while(i < numParams){
		paramTot += param[i];
		++i;
	}

	// Defined by the AX12 Datasheet
	param[len - 2] = ((uint8_t) ~(id + len + inst + paramTot));
}



//===============Commands to control the servos=======================================

// Changes the LED of the servo
void DynaServo::changeLED(uint8_t isOn){
	char buffer[50];
	TXMessage *msg = (TXMessage *)buffer;

	msg->startchunk = 0xffff;
	msg->id = id;
	msg->len = LED_MSG_LEN;
	msg->inst = AX_WRITE_DATA;
	msg->param[0] = AX_LED;
	msg->param[1] = isOn;

	transmit(msg);
}


// Returns true if the LED is on and false if it isn't
BOOL DynaServo::checkLEDStatus(){
	RXMessage RXMsg = readData(AX_LED,1);
	return (bool) RXMsg.param[0];
}

// Reads the current location from the servo registers
// !!!WARNING: Currently this does not return reliable data
void DynaServo::servoTest(){
	RXMessage RXMsg = readData(AX_PRESENT_LOAD_H,1);
	printf("Pos_L %x\r\n", RXMsg.param[0]);
	printf("Pos_H %x\r\n", RXMsg.param[1]);
	printf("Position: %i",((uint16_t) RXMsg.param[0] | ((uint16_t) RXMsg.param[1]) << 8));
	return;
}

// Returns the position, speed and load in that order
// !!!WARNING: Currently this does not return reliable data
DynaServo::ServoStatus DynaServo::getServoStatus(){
	RXMessage RXMsg = readData(AX_PRESENT_POSITION_L,6);
	ServoStatus stat;

	stat.position = (uint16_t) RXMsg.param[0] | ((uint16_t) RXMsg.param[1]) << 8;
//	printf("This is stat.position: %x\r\n", RXMsg.param[0]);
	stat.speed = RXMsg.param[2] | ((uint16_t) RXMsg.param[3]) << 8;
	stat.loadVal = RXMsg.param[4] | ((uint16_t) (RXMsg.param[5] & 0x03)) << 8;
	stat.loadDir = RXMsg.param[5] & 0x04;

	return stat;
}


// Prevents the servo from going to a position
uint16_t DynaServo::checkServoPos(uint8_t idServo,uint16_t position){
	// This servo has a much more limited range because it has a gripper attached to
	// the end
	if(idServo==3){
		if(position > MAX_SERVO3_ANGLE){
			position = MAX_SERVO3_ANGLE;
		}
		else if(position < MIN_SERVO3_ANGLE){
			position = MIN_SERVO3_ANGLE;
		}

	}
	else{
		// Bounds the maximum servo angle
		if(position > MAX_ANGLE){
			position = MAX_ANGLE;
		}
	}
	return position;
}



// Position must be between 0 to 1023 or else it will wrap around
// If speed is 0, the servo will move at max speed
// Speed must be set from 0-1023 or else it will max out at 1023
void DynaServo::moveServo(uint16_t position, uint16_t speed){

	position = checkServoPos(id, position);

	char buffer[50];
	TXMessage *msg = (TXMessage *)buffer;

	msg->startchunk = 0xffff;
	msg->id = id;
	msg->len = MOVE_SERVO_MSG_LEN;
	msg->inst = AX_WRITE_DATA;
	msg->param[0] = AX_GOAL_POSITION_L;
	msg->param[1] = position & 0x00ff;
	msg->param[2] = (position & 0xff00)>>8;
	msg->param[3] = speed & 0x00ff;
	msg->param[4] = (speed & 0xff00)>>8;


	msg->printTX();
	transmit(msg);
}

// Should only be called by the DynaServo that has the ID 0xFE
// This function will move all the servos
// Will pass 0 if the function failed and 1 if successful
// Positions and speeds are arrays with 5 elements that correspond to the
// positions and speeds of the servos
BOOL DynaServo::moveAllServos(uint16_t* positions, uint16_t* speeds){

	if(id == ALL_SERVO_ID){
		char buffer[50];
		TXMessage *msg = (TXMessage *)buffer;

		msg->startchunk = 0xffff;
		msg->id = id;
		msg->len = MOVE_ALL_SERVO_MSG_LEN;
		msg->inst = AX_SYNC_WRITE;
		msg->param[0] = AX_GOAL_POSITION_L;
		msg->param[1] = MOVE_ALL_DATA_LEN;

		for(int i = 0; i < NUM_SERVOS; i++){

			uint16_t position = checkServoPos(i, positions[i]);

			// This sets id of the servo
			msg->param[i * (MOVE_ALL_DATA_LEN + 1) + 2] 	= i + 1;

			msg->param[i * (MOVE_ALL_DATA_LEN + 1) + 3] = position & 0x00ff;
			msg->param[i * (MOVE_ALL_DATA_LEN + 1) + 4] = (position & 0xff00) >> 8;
			msg->param[i * (MOVE_ALL_DATA_LEN + 1) + 5] = speeds[i] & 0x00ff;
			msg->param[i * (MOVE_ALL_DATA_LEN + 1) + 6] = (speeds[i] & 0xff00) >> 8;
		}
		transmit(msg);

		return 1;
	}
	// If the servo id is not correct the function fails
	return 0;


}

//===============Print Statements for Debugging=======================================
void DynaServo::TXMessage::printTX(){
	printf("print TX message: id: %x, len: %x, inst: %x\r\n", id, len, inst);

	uint8_t i = 0;
	uint8_t numParams = len - 2;

	while(i < numParams){
		printf("param[%i]: %x, ", i, param[i]);
		++i;
	}
	iprintf("\r\n");
}


void DynaServo::RXMessage::printRX(){
	printf("print RX message: startChunk:%x, id: %x, len: %x, error: %x\r\n", startchunk, id, len, error);

	uint8_t i = 0;
	uint8_t numParams = len - 2;

	//Prints the parameters and the checksum
	while(i < numParams + 1){
		printf("param[%i]: %x, ", i, param[i]);
		++i;
	}
	iprintf("\r\n");
}

void DynaServo::printServoStatus(){
	ServoStatus stats = getServoStatus();
	printf("Print Servo Status: position: %x, speed: %x, loadDir: %x, loadVal: %x\r\n",
			stats.position, stats.speed, stats.loadDir, stats.loadVal);
}






